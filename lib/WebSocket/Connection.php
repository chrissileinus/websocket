<?php
/* WebSocket/Server.php - Class for Serving a WebSocket for multiple clients in PHP
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Connection  Handles the connection
 */

namespace	WebSocket;

class	Connection {
	protected	$param;
	protected	$socket;
	protected	$buffer			=	null;
	protected	$hash				=	null;
	protected	$customLog	=	null;
	protected	$lastOpCode	=	null;
	protected	$closeStatus=	null;
	protected	$isClosing	=	false;
	protected	$isConnected=	false;
	protected	$isIdle			=	false;

	private		$readable		=	['hash', 'headKey', 'headHost', 'headUri', 'clientHost', 'socket', 'isIdle'];
	private		$headKey		=	null;
	private		$headHost		=	null;
	private		$headUri		=	null;
	private		$clientHost	=	null;

	protected	static	$opCodes	=	[
		'continuation'=>	0,
		'text'				=>	1,
		'binary'			=>	2,
		'close'				=>	8,
		'ping'				=>	9,
		'pong'				=>	10,
	];

	private					function	writeLog($msg, $category = 'error') {
		if($this->customLog)
			call_user_func($this->customLog, $msg, $category);
	}

	final	public		function	__construct($listening, array $param, callable $customLog) {
		$this->param			=	$param;
		$this->customLog	=	$customLog;

		if(empty($this->param['timeout'])) {
			$this->socket	=	@stream_socket_accept($listening, 0.02, $this->clientHost);
		}
		else {
			$this->socket	=	@stream_socket_accept($listening, $this->param['timeout'], $this->clientHost);
		}

		if(!$this->socket) {
			throw	new	Exception('Failed!');
		}

		if(!empty($this->param['timeout'])) {
			stream_set_timeout($this->socket, 0, 1000);
		}

		// stream_set_blocking($this->socket, false);

		$this->handshake($this->socket);
	}

	final	protected	function	handshake() {
		$request	=	'';
		do {
			$request	.=	stream_get_line($this->socket, 1024, "\r\n") . "\n";
		} while(!feof($this->socket) && stream_get_meta_data($this->socket)['unread_bytes'] > 0);

		if($request == "\n") {
			throw	new	ExceptionZero();
		}
		if(!preg_match('/GET (.*) HTTP\//mUi', $request, $matches)) {
			throw	new	Exception("No GET in request");
		}
		$this->headUri	=	parse_url(trim($matches[1]));
		$this->headUri['query']	=	@$this->headUri['query']?:	'';
		parse_str($this->headUri['query'], $this->headUri['array']);

		preg_match('#Host:\s(.*)$#mUi', $request, $matches);
		$this->headHost	=	trim($matches[1]);

		if(!preg_match('#Sec-WebSocket-Key:\s(.*)$#mUi', $request, $matches)) {
				throw new Exception("Client had no Key in upgrade request");
		}

		$this->headKey	=	$key	=	trim($matches[1]);

		/// @todo Validate key length and base 64...
		$response_key	=	base64_encode(pack('H*', sha1($key . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));

		$header	=	"HTTP/1.1 101 Switching Protocols\r\n"
						.	"Upgrade: websocket\r\n"
						.	"Connection: Upgrade\r\n"
						. "Sec-WebSocket-Version: 13\r\n"
						.	"Sec-WebSocket-Accept: $response_key\r\n"
						.	"\r\n";

		$this->write($header);
		$this->isConnected	=	true;
	}

	final	public		function	isConnected() {
		return	$this->isConnected;
	}

	final	public		function	isIdle() {
		return	$this->isIdle;
	}

	/**
	* Tell the socket to close.
	*
	* @param integer $status  http://tools.ietf.org/html/rfc6455#section-7.4
	* @param string  $message A closing message, max 125 bytes.
	*/
	final	public		function	close($status = 1000, $message = 'ttfn') {
		$statusBin	=	sprintf('%016b', $status);
		$statusStr	=	'';
		foreach (str_split($statusBin, 8) as $bin) {
			$statusStr	.=chr(bindec($bin));
		}
		$this->send($statusStr . $message, 'close', true);

		$this->is_closing	=	true;

		return	$this->receive(); // Receiving a close frame will close the socket now.
	}


	final	protected	function	write($data) {
		if(!@stream_get_meta_data($this->socket)) {
			throw	new	Exception("Lost connection!");
		}

		$length	=	@fwrite($this->socket, $data);

		if ($length < strlen($data)) {
			throw	new	Exception("Could only write $length out of " . strlen($data) . " bytes." . var_export($data, true) . var_export(stream_get_meta_data($this->socket), true));
		}
	}

	final	protected	function	read(int $length) {
		$data	=	'';
		while(strlen($data) < $length) {
			$buffer	=	fread($this->socket, $length - strlen($data));

			if($buffer === false) {
				throw	new	Exception(
					'Broken frame, read ' . strlen($data) . ' of stated '
					. $length . ' bytes.  Stream state: '
					. json_encode(stream_get_meta_data($this->socket))
				);
			}
			if($buffer == '') {
				throw	new	Exception(
					'Empty read; connection dead?  Stream state: '
					. json_encode(stream_get_meta_data($this->socket))
				, 1);
			}

			$data	.=	$buffer;
		}
		return	$data;
	}

	final	public		function	sendForced($data, $opCode = 'text', $masked = false) {
		return	$this->send($data, $opCode, $masked, true);
	}
	final	public		function	send($data, $opCode = 'text', $masked = false, bool $ignorHash = false) {
		$hash		=	md5($data);
		if($this->hash	==	$hash && !$ignorHash) {
			//$this->writeLog("\e[34mnothing to send     \e[0m(Key: {$this->headKey})", 'communication');
			return	null;
		}
		$this->hash	=	$hash;

		if(!in_array($opCode, array_keys(self::$opCodes))) {
			throw	new	Exception("Bad opcode '$opCode'.  Try 'text' or 'binary'.");
		}

		$dataLength	=	strlen($data);
		$partCursor	=	0;

		while($dataLength > $partCursor) {
			$subData	=	substr($data, $partCursor, $this->param['partSize']);
			$partCursor	+=	$this->param['partSize'];

			$this->sendPart($dataLength <= $partCursor, $subData, $opCode, $masked);

			$opCode		=	'continuation';
		}
		$this->writeLog("\e[34msend to             \e[0m(Key: {$this->headKey}) ". var_export($data, true), 'communication');
		return	true;
	}

	final	protected	function	sendPart($final, $data, $opCode, $masked) {
		// Binary string for header.
		$head	=	'';

		// Write FIN, final fragment bit.
		$head	.=(bool)	$final	?	'1'	:	'0';

		// RSV 1, 2, & 3 false and unused.
		$head	.='000';

		// Opcode rest of the byte.
		$head	.=sprintf('%04b', self::$opCodes[$opCode]);

		// Use masking?
		$head	.=$masked ? '1' : '0';


		// 7 bits of data length...
		$dataLength			=	strlen($data);
		if($dataLength > 65535) {
			$head	.=decbin(127);
			$head	.=sprintf('%064b', $dataLength);
		}
		elseif($dataLength > 125) {
			$head	.=decbin(126);
			$head	.=sprintf('%016b', $dataLength);
		}
		else{
			$head	.=sprintf('%07b', $dataLength);
		}

		$frame	=	'';

		// Write head to frame.
		foreach(str_split($head, 8) as $binstr) {
			$frame	.=chr(bindec($binstr));
		}

		// Handle masking
		if($masked) {
			// generate a random mask:
			$mask	=	'';
			for($i = 0; $i < 4; $i++) {
				$mask	.=chr(rand(0, 255));
			}
			$frame	.=$mask;
		}

		// Append payload to frame:
		for($i = 0; $i < $dataLength; $i++) {
			$frame	.=($masked === true)?	$data[$i] ^ $mask[$i % 4]:	$data[$i];
		}

		try {
			$this->write($frame);
		} catch (Exception $e) {
			//var_dump($e);
			$this->isConnected	=	false;
		}
	}

	final	public		function	receive() {
		$this->buffer	=	'';

		$data	=	null;
		while(is_null($data)) {
			$data	=	$this->receivePart();
		}
		$this->writeLog("\e[96mreceived from       \e[0m(Key: {$this->headKey}) ". var_export($data, true), 'communication');

		$tmp	=	json_decode($data, JSON_OBJECT_AS_ARRAY);
		if(isset($tmp['idle'])) {
			if($tmp['idle']) {
				$this->writeLog("Client idle \e[0m(Key: {$this->headKey})", 'connection');
				$this->isIdle	=	true;
				return	null;
			}
			else {
				$this->writeLog("Client aktive \e[0m(Key: {$this->headKey})", 'connection');
				$this->isIdle	=	false;
				return	null;
			}
		}
		if(isset($tmp['switchRoute'])) {
			$this->headUri['path'] = $tmp['switchRoute'];
			$this->writeLog("Client has new route '{$this->headUri['path']}' \e[0m(Key: {$this->headKey})", 'connection');
		}

		$this->isIdle	=	false;

		return	$data;
	}

	final	protected	function	receivePart() {

		// Just read the main fragment information first.
		$head		=	$this->read(2);

		// Is this the final fragment?  // Bit 0 in byte 0
		/// @todo Handle huge payloads with multiple fragments.
		$final	=	(bool)	(ord($head[0]) & 1 << 7);

		// Should be unused, and must be false…  // Bits 1, 2 & 3
		$rsv1		=	(bool)	(ord($head[0]) & 1 << 6);
		$rsv2		=	(bool)	(ord($head[0]) & 1 << 5);
		$rsv3		=	(bool)	(ord($head[0]) & 1 << 4);

		// Parse opcode
		$opCodeInt		=	(ord($head[0]) & 31); // Bits 4-7
		$opCodeInts		=	array_flip(self::$opCodes);
		if(!array_key_exists($opCodeInt, $opCodeInts)) {
			throw new Exception("Bad opcode in websocket frame: $opCodeInt");
		}
		$opCode	=	$opCodeInts[$opCodeInt];

		// record the opcode if we are not receiving a continuation fragment
		if($opCode !== 'continuation') {
			$this->lastOpCode	=	$opCode;
		}

		// Masking?
		$mask	=	(bool)	(ord($head[1]) >> 7);  // Bit 0 in byte 1

		// Payload length
		$dataLength	=	(int)	ord($head[1]) & 127; // Bits 1-7 in byte 1
		if($dataLength > 125) {
			if($dataLength === 126)	$head	=	$this->read(2); // 126: Payload is a 16-bit unsigned int
			else										$head	=	$this->read(8); // 127: Payload is a 64-bit unsigned int
			$dataLength	=	bindec(self::sprintB($head));
		}

		// Get masking key.
		if($mask) {
			$maskingKey	=	$this->read(4);
		}

		$data	=	null;

		// Get the actual payload, if any (might not be for e.g. close frames.
		if($dataLength > 0) {
			$tmp	=	$this->read($dataLength);

			if($mask) {
				// Unmask payload.
				for($i = 0; $i < $dataLength; $i++) {
					$data	.=($tmp[$i] ^ $maskingKey[$i % 4]);
				}
			}
			else {
				$data	=	$tmp;
			}
		}

		//var_dump($opCode);

		if($opCode === 'close') {
			// Get the close status.
			if($dataLength >= 2) {
				$status_bin					=	$data[0] . $data[1];
				$status							=	bindec(sprintf("%08b%08b", ord($data[0]), ord($data[1])));
				$this->closeStatus	=	$status;
				$data								=	substr($data, 2);

				if(!$this->isClosing) {
					$this->send($status_bin . 'Close acknowledged: ' . $status, 'close', true); // Respond
				}
			}

			if($this->isClosing)
				$this->isClosing	=	false; // A close response, all done.

			// And close the socket.
			fclose($this->socket);
			$this->isConnected	=	false;
		}

		// if this is not the last fragment, then we need to save the payload
		if(!$final) {
			$this->buffer	.=$data;
			return	null;
		}
		// this is the last fragment, and we are processing a buffer
		elseif($this->buffer) {
			// sp we need to retrieve the whole payload
			$data			=	$this->buffer	.=$data;
			$this->buffer	=	null;
		}

		return	$data;
	}

	/**
	* Helper to convert a binary to a string of '0' and '1'.
	*/
	final	protected	static	function	sprintB($string) {
		$return	=	'';
		for($i = 0; $i < strlen($string); $i++) {
			$return	.=sprintf("%08b", ord($string[$i]));
		}
		return	$return;
	}

	final	public		function	__get($key) {
		if(in_array($key, $this->readable))
			return	$this->$key;
		else
			return	null;
	}

}

?>