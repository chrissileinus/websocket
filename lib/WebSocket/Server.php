<?php
/* WebSocket/Server.php - Class for Serving a WebSocket for multiple clients in PHP
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

/* Parts:
 *   class Server      Wait for new connections
 */

namespace	WebSocket;

class	Server {
	protected	$param					=	[];
	protected	$listening			=	false;
	protected	$connections		=	[];
	protected	$sockets				=	[];
	protected	$lastIneterval	=	0;
	protected	$allIdle				=	false;

	//	__construct	START	-------------------------------------------------------
	final	public		function	__construct(array $param = []) {
		$this->param['address']		=	strval(				@$param['address']		??	"tcp://0.0.0.0:9500");
		$this->param['timeout']		=	floatval(max(	@$param['timeout']		,	0.001));
		$this->param['partSize']	=	intval(				@$param['partSize']		??	1024*4);
		$this->param['logFilter']	=								@$param['logFilter']	??	['communication', 'connection', 'error'];

		$this->lastInterval	=	microtime(true);

		$this->listening		=	stream_socket_server($this->param['address'], $errno, $errstr);
		if(!$this->listening) {
			throw	new	Exception("Could not open listening socket.");
		}
		else {
			$this->writeLog("Listening on {$this->param['address']}");
		}

		//var_dump($this->param);
	}
	//	__construct	END		-------------------------------------------------------


	//	__destruct	START	-------------------------------------------------------
	final	public		function	__destruct() {
		foreach($this->connections as $key => $connection) {
			try {
				$connection->close($status = 1000, $message = 'Close service');
			} catch(Exception $e) {}
		}
	}
	//	__destruct	END		-------------------------------------------------------


	/*	on*			START	-------------------------------------------------------
	 *	This are placeholder an get called on different situations
	 *	They should be overwirten by the user application.
	*/
	public					function	onConnect($client) {}
	public					function	onDisconnect($client) {}
	public					function	onReceive($client, $data) {}
	public					function	onTick() {}
	public					function	onIdle() {}
	public					function	onActive() {}

	public					function	log($msg) {}
	//	on*			END		-------------------------------------------------------

	/*	writeLog	START	-------------------------------------------------------
	 *	Write in to log using the user logging function
	*/
	public					function	writeLog($msg, $category = 'error') {
		if(in_array($category, $this->param['logFilter'])) {
			$this->log($msg);
		}
	}
	//	writeLog	END		-------------------------------------------------------

	/*	run			START	-------------------------------------------------------
	 *	This method is to acct as a expression in while.
	 *	The callback runs for all connections once and get the Connection Object to work with.
	 *
	 *	Example:
	 *	while(WebSocket\Server->run()) {}
	*/
	final	public		function	run(float $timeout = 0) {
		$read		=	array_merge($this->sockets, [$this->listening]);
		$write	=	null;
		$except	=	null;
		if(@\stream_select($read, $write, $except, 0, $timeout * 1000000) > 0) {
			foreach($read as $socket) {
				if($socket === $this->listening) {
					try {
						$this->connections[]	=	$connection	=	new	Connection($socket, $this->param, [$this, 'writeLog']);
						@$this->sockets[]			=	&$connection->socket;
		
						$this->writeLog("\e[32mClient connected    \e[0m(Key: {$connection->headKey} Client: {$connection->clientHost} Host: {$connection->headHost} Path: {$connection->headUri['path']} Query: {$connection->headUri['query']})", 'connection');
						$this->onConnect($connection);
					} catch(Exception $e) {
						$this->writeLog("\e[31mError new connection  \n\e[0m(Key: {$connection->headKey} Client: {$connection->clientHost} Host: {$connection->headHost} Path: {$connection->headUri['path']} Query: {$connection->headUri['query']})  \n-<(\e[0m{$e->getCode()}: '{$e->getMessage()}' in '{$e->getFile()}' on Line {$e->getLine()}\e[31m)>-\e[0m");
					} catch(ExceptionZero $e) {
					}
				}
				else {
					foreach($this->connections as $key => $connection) {
						if($connection->socket === $socket) {
							try {
								$this->onReceive($this->connections[$key], $this->connections[$key]->receive());
							} catch(Exception $e) {
								$this->writeLog("\e[31mError on receive  \n\e[0m(Key: {$connection->headKey} Client: {$connection->clientHost} Host: {$connection->headHost} Path: {$connection->headUri['path']} Query: {$connection->headUri['query']})  \n-<(\e[0m{$e->getCode()}: '{$e->getMessage()}' in '{$e->getFile()}' on Line {$e->getLine()}\e[31m)>-\e[0m");
			
								if($e->getCode() >= 1) {
									$this->onDisconnect($this->connections[$key]);
									$this->writeLog("\e[31mClient disconnected \e[0m(Key: {$this->connections[$key]->headKey} Client: {$this->connections[$key]->clientHost} Host: {$this->connections[$key]->headHost})", 'connection');
									unset($this->connections[$key]);
									unset($this->sockets[$key]);
								}
							}
						}
					}
				}
			}
			
			$allIdle	=	true;
			foreach($this->connections as $connection) {
				if(!$connection->isIdle()) {
					$allIdle	=	false;
				}
			}
			if($allIdle != $this->allIdle) {
				if($allIdle) {
					$this->writeLog("all Clients are in idle", 'connection');
					$this->onIdle();
				}
				else {
					$this->writeLog("Clients are aktive", 'connection');
					$this->onActive();
				}
			}
			$this->allIdle	=	$allIdle;
		}

		$this->onTick();

		$this->destroyLostConnections();
		return	true;
	}

	final	protected	function	destroyLostConnections() {
		foreach($this->connections as $key => $connection) {
			if(!$connection->isConnected()) {
				$this->onDisconnect($connection);
				$this->writeLog("\e[31mClient destroyed \e[0m(Key: {$connection->headKey} Client: {$connection->clientHost} Host: {$connection->headHost})", 'connection');
				unset($this->connections[$key]);
				unset($this->sockets[$key]);
			}
		}
	}
	//	run			END		-------------------------------------------------------

	/*	sendBroadcast	START	-------------------------------------------------------
	 *	Send a Massage to all connection
	*/
	final	public		function	sendBroadcast($data, bool $ignorHash = false, $route = NULL) {
		$count	=	0;
		if(!is_string($data))
			$data	=	json_encode($data);

		foreach($this->connections as $key => $connection) {
			if(
				$route === null ||
				(is_string($route) && $connection->headUri['path'] == $route) || 
				(is_array($route)  && array_search($connection->headUri['path'], $route) !== false)
			) {
				if($ignorHash)	{
					if($connection->sendForced($data))	$count++;
				}
				else			{
					if($connection->send($data))		$count++;
				}
			}
		}
		return	$count;
	}
	//	sendBroadcast	END		-------------------------------------------------------

	/*	sendTo	START	-------------------------------------------------------
	 *	Send a Massage to connection with headKey....
	*/
	final	public		function	sendTo($data, $headKey) {
		$count	=	0;
		if(!is_string($data))
			$data	=	json_encode($data);

		foreach($this->connections as $key => $connection) {
			if($connection->headKey == headKey) {
				if($connection->send($data))		$count++;
			}
		}
		return	$count;
	}
	//	sendTo	END		-------------------------------------------------------

	final	public		function	allIdle() {
		return	$this->allIdle;
	}

	final	public		function	countConnections() {
		return	count($this->connections);
	}

	final	public		function	getAllReadSockets() {
		return	array_merge($this->sockets, [$this->listening]);
	}
}

?>