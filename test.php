<?php
require __DIR__ . '/vendor/autoload.php';

$wss	=	new	class(['interval' => 0.1]) extends \WebSocket\Server {
	public	function	onConnect($client) {
		$this->sendBroadcast(json_encode(['new',$client->headKey]));
	}

	public	function	onDisconnect($client) {
		$this->sendBroadcast(json_encode(['lost',$client->headKey]));
	}

	public	function	onReceive($client, $data) {
		global	$tmp;
		$tmp[]	=	$data;
	}

	public	function	log($msg) {
		echo $msg.PHP_EOL;
	}
};

$tmp	=	[];

$mtime	=	microtime(true);

while($wss->run()) {

	if($tmp_ = array_shift($tmp)) {
		//var_dump($tmp_);
		$wss->sendBroadcast($tmp_);
		unset($tmp_);
	}

	//$wss->sendAll("hallo\n");

	//var_dump($wss->countConnections());

	$mtime_	=	microtime(true);
	//echo($mtime_ - $mtime . PHP_EOL);
	$mtime	=	$mtime_;

	//sleep(1);
}

?>